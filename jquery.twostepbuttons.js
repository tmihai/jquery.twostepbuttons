/*
 * Copyright 2013
 * Tiberiu Mihai
 * tiberiu.mihai@gmail.com
*/

(function($)
{
    $.fn.twostepbutton = function( options )
    {
        // Establish our default settings
        var settings = $.extend({
            text              : 'Adauga in cos',
            color             : '#EFEFEF',
            background        : '#444444',
            hoverBackground   : '#747474',
            padding           : null,
            fontStyle         : null,
            fontSize          : '12',
            width             : null,
            display           : 'inline-block',
            textAlign         : 'center',
            complete          : null
        }, options);

        return this.each( function()
        {
            $(this).click(function()
            {
                if( !$(this).attr('face') )
                {
                    $(this).attr('face', '1');
                }

                if( $(this).attr('face') == '1' )
                {
                    $(this).slideUp(100, function() {
                        // Animation complete
                        $(this).text( settings.text ).slideDown('fast');;
                        $(this).attr('face', '2');

                        if ( settings.hoverBackground ) 
                        {
                            $(this).css( 'background-color', settings.hoverBackground );
                        }
                    });
                }
                else
                {
                    $(this).slideUp(200, function() {
                        // Animation complete
                        $(this).text( $(this).attr('value') ).slideDown('fast');
                        $(this).attr('face', '1');

                        if ( settings.background ) 
                        {
                            $(this).css( 'background-color', settings.background );
                        }
                    });

                    if ( $.isFunction( settings.complete ) ) 
                    {
                        settings.complete.call( this );
                    }
                }
            });

/*
            $(this).mouseenter(function()
            {
                if ( settings.hoverBackground ) 
                {
                    $(this).css( 'background-color', settings.hoverBackground );
                }
            });

            $(this).mouseleave(function()
            {
                if ( settings.background ) 
                {
                    $(this).css( 'background-color', settings.background );
                }
            });
*/

            if ( settings.color ) 
            {
                $(this).css( 'color', settings.color );
            }
            
            if ( settings.background ) 
            {
                $(this).css( 'background-color', settings.background );
            }
            
            if ( settings.padding ) 
            {
                $(this).css( 'padding', settings.padding );
            }

            if ( settings.fontStyle ) 
            {
                $(this).css( 'font-style', settings.fontStyle );
            }
            
            if ( settings.fontSize )
            {
                $(this).css( 'font-size', settings.fontSize );
            }
            
            $(this).css( 'display', settings.display );
            if ( settings.width ) 
            {
                $(this).css( 'width', settings.width );
            }
            
            $(this).css( 'text-align', settings.textAlign );
            $(this).css( 'text-decoration', 'none' );
            $(this).css( 'border', '0' );
        });

    }

}(jQuery));